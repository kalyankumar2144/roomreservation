package com.tringapps.roomreservation.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the EmpDeptMapping database table.
 * 
 */
@Entity
@NamedQuery(name="EmpDeptMapping.findAll", query="SELECT e FROM EmpDeptMapping e")
public class EmpDeptMapping implements Serializable {
	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private EmpDeptMappingPK id;

	@Column(name="created_by", nullable = false)
	private String createdBy;

	@Column(name="created_time", nullable = false)
	private long createdTime;

	@Column(name="is_deleted", nullable = false)
	private Boolean isDeleted;

	@Column(name="updated_by", nullable = false)
	private String updatedBy;

	@Column(name="updated_time", nullable = false)
	private long updatedTime;

	public EmpDeptMapping() {
	}

	public EmpDeptMappingPK getId() {
		return this.id;
	}

	public void setId(EmpDeptMappingPK id) {
		this.id = id;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public long getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public long getUpdatedTime() {
		return this.updatedTime;
	}

	public void setUpdatedTime(long updatedTime) {
		this.updatedTime = updatedTime;
	}

}