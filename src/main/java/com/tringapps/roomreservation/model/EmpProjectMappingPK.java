package com.tringapps.roomreservation.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the EmpProjectMapping database table.
 * 
 */
@Embeddable
public class EmpProjectMappingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="project_id")
	private int projectId;

	@Column(name="emp_id")
	private int empId;

	public EmpProjectMappingPK() {
	}
	public int getProjectId() {
		return this.projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public int getEmpId() {
		return this.empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EmpProjectMappingPK)) {
			return false;
		}
		EmpProjectMappingPK castOther = (EmpProjectMappingPK)other;
		return 
			(this.projectId == castOther.projectId)
			&& (this.empId == castOther.empId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.projectId;
		hash = hash * prime + this.empId;
		
		return hash;
	}
}