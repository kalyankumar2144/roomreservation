package com.tringapps.roomreservation.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the EmpDeptMapping database table.
 * 
 */
@Embeddable
public class EmpDeptMappingPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="emp_id")
	private int empId;

	@Column(name="dept_id")
	private int deptId;

	public EmpDeptMappingPK() {
	}
	public int getEmpId() {
		return this.empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public int getDeptId() {
		return this.deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof EmpDeptMappingPK)) {
			return false;
		}
		EmpDeptMappingPK castOther = (EmpDeptMappingPK)other;
		return 
			(this.empId == castOther.empId)
			&& (this.deptId == castOther.deptId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.empId;
		hash = hash * prime + this.deptId;
		
		return hash;
	}
}