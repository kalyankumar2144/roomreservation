package com.tringapps.roomreservation.webservice;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.exception.ConstraintViolationException;

import com.tringapps.roomreservation.domain.model.ErrorResponse;
import com.tringapps.roomreservation.dto.RoomDto;
import com.tringapps.roomreservation.helpers.CustomResponse;
import com.tringapps.roomreservation.model.Room;
import com.tringapps.roomreservation.service.RoomService;

import io.swagger.annotations.Api;

@Path("/room")
@Api(value = "room")
public class RoomWebService {
	
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create")
	public CustomResponse createRoom(RoomDto roomDto, @Context HttpServletResponse response) throws Exception {

		try {
			RoomService roomService = new RoomService();
			roomService.createRoomEntry(roomDto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Room added successfully!");
		} catch (ConstraintViolationException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Room already exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/roomdetail")
	public RoomDto getRoomDetail(@QueryParam(value = "roomNumber") Integer roomNumber, @Context HttpServletResponse response) throws Exception {
		
		try {
			RoomService roomService = new RoomService();
			Room room = roomService.getRoomDetail(roomNumber);
			response.setStatus(HttpServletResponse.SC_OK);
		    response.flushBuffer();
			return this.formResponse(room);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/roomdetails")
	public Response getRoomDetails() {
		
		try {
			RoomService roomService = new RoomService();
			List<Room> roomList = roomService.getRoomDetails();
			Response response = Response.status(200).entity(roomList).build();
			return response;
			
		} catch (Exception exception) {
			ErrorResponse response = new ErrorResponse();
			response.message = "Internal error --> "+exception.getLocalizedMessage();
			Response actualresponse = Response.status(500).entity(response).build();
			return actualresponse;
		}
		
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/updateroomdetails")
	public Response updateRoomDetails(RoomDto roomDto) {
		
		try {
			RoomService roomService = new RoomService();
			Room room = roomService.updateRoomDetail(roomDto);
			Response response = Response.status(200).entity(this.formResponse(room)).build();
			return response;
			
		} catch (Exception exception) {
			ErrorResponse response = new ErrorResponse();
			response.message = "Internal error --> "+exception.getLocalizedMessage();
			Response actualresponse = Response.status(500).entity(response).build();
			return actualresponse;
		}
		
		
	}
	
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/deleteroomdetail")
	public Response deleteRoomDetail(@QueryParam(value = "roomNumber") Integer roomNumber) {
		try {
			RoomService roomService = new RoomService();
			roomService.deleteRoomDetail(roomNumber);
			Response response = Response.status(200).entity("Success").build();
			return response;
		} catch (Exception exception) {
			ErrorResponse response = new ErrorResponse();
			response.message = "Internal error --> "+exception.getLocalizedMessage();
			Response actualresponse = Response.status(500).entity(response).build();
			return actualresponse;
		}
	}
	
	
	
private RoomDto formResponse(Room room) {
	
	RoomDto response = new RoomDto();
	response.setRoomCapacity(room.getCapacity());
	response.setRoomNumber(room.getNumber());
	response.setRoomName(room.getName());
	return response;
	}
}
