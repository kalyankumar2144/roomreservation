package com.tringapps.roomreservation.webservice;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.exception.ConstraintViolationException;

import com.tringapps.roomreservation.dto.DepartmentDTO;
import com.tringapps.roomreservation.dto.DepartmentResponse;
import com.tringapps.roomreservation.helpers.CustomResponse;
import com.tringapps.roomreservation.service.DepartmentService;

import io.swagger.annotations.Api;

@SuppressWarnings("all")

@Path("department")
@Api(value = "department")
public class DepartmentWebService {

	DepartmentService deptService = new DepartmentService();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("create")
	public CustomResponse createDepartment(DepartmentDTO departmentDto,  @Context HttpServletResponse response) throws Exception {
		try {
			deptService.createDepartment(departmentDto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Department created successfully");
		} catch (ConstraintViolationException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Department already exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	@GET
	@Path("getdepartments")
	@Produces(MediaType.APPLICATION_JSON)
	public DepartmentResponse getDepartments(@Context HttpServletResponse response) throws Exception {
		try {
			DepartmentResponse department = new DepartmentResponse();
			department.setDepartmentList(deptService.getDepartments());
			response.setStatus(HttpServletResponse.SC_OK);
		    response.flushBuffer();
			return department;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	@POST
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CustomResponse updateDepartment(DepartmentDTO dto, @Context HttpServletResponse response) throws Exception {
		try {
			deptService.updateDepartment(dto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Department updated successfully");
		}
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Department does not exist!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	@POST
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public CustomResponse deleteDepartment(DepartmentDTO departmentDto, @Context HttpServletResponse response) throws Exception {
		
		try {
			deptService.deleteDepartment(departmentDto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Department deleted successfully");
		}
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Department does not exist!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

}
