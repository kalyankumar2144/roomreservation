package com.tringapps.roomreservation.webservice;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.tringapps.roomreservation.dto.BookingDTO;
import com.tringapps.roomreservation.service.BookingService;

import io.swagger.annotations.Api;



@Path("/booking")
@Api(value = "booking")
public class BookingWebService {

	
	BookingService bookingService = new BookingService();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/create")
	public void createBooking(BookingDTO bookingDto) {
 
		bookingService.createBooking(bookingDto);
		System.out.println("bookedBy:::"+bookingDto.getBookedBy());
 
	}
	
}



