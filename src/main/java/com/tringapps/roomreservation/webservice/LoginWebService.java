package com.tringapps.roomreservation.webservice;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.core.util.Base64;
import com.tringapps.roomreservation.dto.EmployeeDTO;
import com.tringapps.roomreservation.dto.LoginDto;
import com.tringapps.roomreservation.dto.LoginResponse;
import com.tringapps.roomreservation.filter.AuthenticationFilter;
import com.tringapps.roomreservation.helpers.CustomResponse;
import com.tringapps.roomreservation.model.Employee;
import com.tringapps.roomreservation.model.UserSession;
import com.tringapps.roomreservation.service.EmployeeService;
import com.tringapps.roomreservation.service.SessionService;

import io.swagger.annotations.Api;

@Path("user")
@Api(value = "user")

public class LoginWebService {
	
	/***** Login Employee *****/

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("login")
	public CustomResponse login(LoginDto dto, @Context HttpServletResponse response, @Context HttpServletRequest request) throws Exception {
		try {
			EmployeeService service = new EmployeeService();
			Employee employee = service.findEmployee(dto.getEmail());
			if (employee.getPassword().equals(dto.getPassword())) {
				
				dto.setDeviceType(request.getHeader(AuthenticationFilter.DEVICE_TYPE_HEADER));
				dto.setDeviceToken(request.getHeader(AuthenticationFilter.DEVICE_TOKEN_HEADER));

				LoginResponse login = createSession(employee, dto);
				response.addHeader(AuthenticationFilter.AUTHENTICATION_HEADER, login.getAuthId());
				request.getSession();
				response.setStatus(HttpServletResponse.SC_OK);
				response.flushBuffer();
				return login;
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				response.flushBuffer();
				return new CustomResponse("Enter valid password!");
			}
		} catch (NoResultException e) {
			e.printStackTrace();
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("User does not exist!");
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/***** Logout Employee *****/

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("logout")
	public CustomResponse logout(EmployeeDTO dto, @Context HttpServletResponse response,  @Context HttpServletRequest request) throws Exception {

		try {
			SessionService session = new SessionService();
			session.logout(dto.getEmail());
			request.getSession().invalidate();
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Logout successfully");
		} 
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("User does not exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	private LoginResponse createSession(Employee employee, LoginDto dto) throws Exception {
		SessionService session = new SessionService();
		try {
			UserSession user = session.findSession(employee.getEmail());
			return formResponse(employee, user.getAuthId());
		}
		catch (NoResultException e) {
			String sessionString = employee.getName() + "|" + employee.getEmail() + "|" + System.currentTimeMillis();
			byte[] bytesEncoded = Base64.encode(sessionString.getBytes());
			dto.setAuthId(new String(bytesEncoded));
			dto.setUserName(employee.getName());
			session.createSession(dto);
			return formResponse(employee, new String(bytesEncoded));
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}	

	/***** Helper methods *****/

	private LoginResponse formResponse(Employee employee, String authId) {
		LoginResponse response = new LoginResponse("Login successfully!");
		response.setEmail(employee.getEmail());
		response.setDesignation(employee.getDesignation());
		response.setName(employee.getName());
		response.setAuthId(authId);
		System.out.println("AuthId === "+authId);
		return response;
	}

}
