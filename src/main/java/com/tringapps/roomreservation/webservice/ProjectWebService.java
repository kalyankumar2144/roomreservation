package com.tringapps.roomreservation.webservice;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.hibernate.exception.ConstraintViolationException;

import javax.servlet.http.HttpServletResponse;

import com.tringapps.roomreservation.dto.ProjectDTO;
import com.tringapps.roomreservation.dto.ProjectResponse;
import com.tringapps.roomreservation.helpers.CustomResponse;
import com.tringapps.roomreservation.service.ProjectService;

import io.swagger.annotations.Api;

@SuppressWarnings("all")

@Path("project")
@Api(value = "project")
public class ProjectWebService {

	ProjectService service = new ProjectService();
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("create")
	public CustomResponse create(ProjectDTO project, @Context HttpServletResponse response) throws Exception {

		try {
			service.create(project);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Project added successfully!");
		} catch (ConstraintViolationException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Project already exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("getprojects")
	public ProjectResponse getProjects(@Context HttpServletResponse response) throws Exception {
		try {
			ProjectResponse project = new ProjectResponse();
			project.setProjectList(service.getProjects());
			response.setStatus(HttpServletResponse.SC_OK);
		    response.flushBuffer();
			return project;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete")
	public CustomResponse delete(ProjectDTO dto, @Context HttpServletResponse response) throws Exception {
		try {
			service.delete(dto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Project deleted successfully");
		}
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("Project does not exist!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
}
