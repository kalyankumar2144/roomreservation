package com.tringapps.roomreservation.webservice;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import io.swagger.jaxrs.config.BeanConfig;

public class SwaggerConfiguration extends HttpServlet{
	   public void init(ServletConfig config) throws ServletException {
	       super.init(config);
	       BeanConfig beanConfig = new BeanConfig();
	       beanConfig.setTitle("Room Reservation");
	       beanConfig.setVersion("1.0");
	       beanConfig.setSchemes(new String[]{"http"});
	       beanConfig.setHost("localhost:8080");
	       beanConfig.setBasePath("/RoomReservation/api");
	      beanConfig.setResourcePackage("com.tringapps.roomreservation.webservice");
	     //   beanConfig.setResourcePackage("io.swagger.resources");
	       beanConfig.setScan(true);
	       beanConfig.setDescription("Room Reservation");
	   }
	}