package com.tringapps.roomreservation.webservice;

import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import java.io.IOException;
import java.lang.Exception;
import java.sql.Date;
import java.util.Random;

import com.sun.jersey.core.util.Base64;
import com.tringapps.roomreservation.dto.EmployeeDTO;
import com.tringapps.roomreservation.helpers.CustomResponse;
import com.tringapps.roomreservation.model.Employee;
import com.tringapps.roomreservation.model.UserSession;
import com.tringapps.roomreservation.service.EmployeeService;

import io.swagger.annotations.Api;

@Path("employee")
@Api(value = "employee")
public class EmployeeWebService {

	EmployeeService service = new EmployeeService();

	/***** Sign up Employee *****/

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("signup")
	public CustomResponse signup(EmployeeDTO dto, @Context HttpServletResponse response) throws Exception {

		try {
			service.findEmployee(dto.getEmail());
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("User already exists!");
		} 
		catch (NoResultException e) {
			e.printStackTrace();
			service.createEmployee(dto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("User signuped successfully");
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/***** Update Employee details *****/

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("update")
	public CustomResponse update(EmployeeDTO dto, @Context HttpServletResponse response) throws Exception {

		try {
			service.updateEmployee(dto);
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Employee updated successfully");
		} 
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("User does not exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/***** Delete Employee details *****/

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("delete")
	public CustomResponse delete(EmployeeDTO dto, @Context HttpServletResponse response) throws Exception {
		try {
			service.deleteEmployee(dto.getEmail());
			response.setStatus(HttpServletResponse.SC_OK);
			response.flushBuffer();
			return new CustomResponse("Employee deleted successfully");
		} 
		catch (NoResultException e) {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			response.flushBuffer();
			return new CustomResponse("User does not exists!");
		}
		catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
		
}
