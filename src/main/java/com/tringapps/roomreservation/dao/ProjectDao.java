package com.tringapps.roomreservation.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.model.Project;
import com.tringapps.roomreservation.dto.ProjectDTO;

@SuppressWarnings("all")

public class ProjectDao {
	
	public void create(ProjectDTO projectDTO, Session session) {
		Project project = new Project();
		project.setName(projectDTO.getName());
		project.setIsDeleted(false);
		// Audit fields
		project.setCreatedBy(System.getProperty("user.name"));
		project.setCreatedTime(System.currentTimeMillis());
		project.setUpdatedBy(System.getProperty("user.name"));
		project.setUpdatedTime(System.currentTimeMillis());
		session.save(project);
	}
	
	public void delete(Session session, String name) throws Exception {
		
		String queryString = "FROM Project where name = '"+name+"'";
		Query query = session.createQuery(queryString);
		Project project = (Project) query.getSingleResult();
		if(project != null) {
			project.setUpdatedBy(System.getProperty("user.name"));
			project.setUpdatedTime(System.currentTimeMillis());
			project.setIsDeleted(true);
			session.update(project);
		}
	}

}
