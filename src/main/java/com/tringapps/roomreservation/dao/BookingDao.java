package com.tringapps.roomreservation.dao;

import org.hibernate.Session;

import com.tringapps.roomreservation.model.Booking;
import com.tringapps.roomreservation.dto.BookingDTO;

public class BookingDao {
	
	public void createBooking(BookingDTO bookingDto,Session session){
		Booking booking = new Booking();
		booking.setBookedBy(bookingDto.getBookedBy() );
		booking.setEndTime(bookingDto.getEndTime());
		booking.setStartTime(bookingDto.getStartTime());
		booking.setIsDeleted(false);
		// Audit fields
		booking.setCreatedBy(System.getProperty("user.name"));
		booking.setCreatedTime(System.currentTimeMillis());
		booking.setUpdatedBy(System.getProperty("user.name"));
		booking.setUpdatedTime(System.currentTimeMillis());
		session.save(booking);
	}
}
