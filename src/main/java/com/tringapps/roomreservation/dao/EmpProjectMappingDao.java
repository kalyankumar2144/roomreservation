package com.tringapps.roomreservation.dao;

import org.hibernate.Session;

import com.tringapps.roomreservation.model.EmpProjectMapping;
import com.tringapps.roomreservation.model.EmpProjectMappingPK;

public class EmpProjectMappingDao {


	public void create(int empId, int projectId, Session session) throws Exception {

		EmpProjectMapping empProj = new EmpProjectMapping();
		EmpProjectMappingPK pk = new EmpProjectMappingPK();
		pk.setEmpId(empId);
		pk.setProjectId(projectId);
		empProj.setId(pk);
		// Audit fields
		empProj.setCreatedBy(System.getProperty("user.name"));
		empProj.setCreatedTime(System.currentTimeMillis());
		empProj.setUpdatedBy(System.getProperty("user.name"));
		empProj.setUpdatedTime(System.currentTimeMillis());
		empProj.setIsDeleted(false);
		session.save(empProj);
	}

}
