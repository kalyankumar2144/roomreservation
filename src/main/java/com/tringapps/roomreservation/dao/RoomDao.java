package com.tringapps.roomreservation.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.model.Room;
import com.tringapps.roomreservation.dto.RoomDto;

@SuppressWarnings("all")

public class RoomDao<SQLQuery> {

	Logger log = Logger.getLogger(RoomDao.class.getName());

	public Room insertRoomDetails(RoomDto roomDto, Session session) {
		log.warning("insertRoomDetails");
		Room room = convertRoomDtoToRoomObject(roomDto);
		session.save(room);
		return room;
	}
	
	Room convertRoomDtoToRoomObject(RoomDto roomDto) {
		
		Room room = new Room();
		room.setNumber(roomDto.getRoomNumber());
		room.setName(roomDto.getRoomName());
		room.setCapacity(roomDto.getRoomCapacity());
		room.setIsDeleted(false);
		// Audit fields
		room.setCreatedBy(System.getProperty("user.name"));
		room.setCreatedTime(System.currentTimeMillis());
		room.setUpdatedBy(System.getProperty("user.name"));
		room.setUpdatedTime(System.currentTimeMillis());
		return room;
	}
	
	public Room getRoomDetail(int roomNumber, Session session) {
		log.warning("getRoomDetail");
		String query_String = "select * from RoomBooking.Room where is_deleted != 1 AND number=";
		String mergstring = query_String + Integer.toString(roomNumber) + ";" ;
		Query query = session.createNativeQuery(mergstring,Room.class);
		List<Room> roomList = new ArrayList<>();
		roomList = query.getResultList();
		Room roomDetail = roomList.get(0);
		log.warning(roomList.toString());
		return roomDetail;
	}
	
	public List getRoomDetails(Session session){
		log.warning("getRoomDetails");
		String query_String = "select * from RoomBooking.Room where is_deleted != 1;" ;
		Query query = session.createNativeQuery(query_String,Room.class);
		List<Room> roomList = new ArrayList<>();
		roomList = query.getResultList();
		return roomList;
	}
	
	public Room updateRoomDetail(RoomDto roomDto, Session session)
	{
		log.warning("updateRoomDetail");
		Room room = convertRoomDtoToRoomObject(roomDto); 
		session.merge(room);
		return room;
	}
	
	public Room deleteRoomDetail(int roomNumber, Session session) {
		log.warning("deleteRoomDetail");
		Room roomDetail =  session.find(Room.class, roomNumber);
		roomDetail.setIsDeleted(true);
		session.merge(roomDetail);
		return roomDetail;
	}
	
}
