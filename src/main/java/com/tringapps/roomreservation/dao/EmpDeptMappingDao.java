package com.tringapps.roomreservation.dao;

import org.hibernate.Session;

import com.tringapps.roomreservation.model.EmpDeptMapping;
import com.tringapps.roomreservation.model.EmpDeptMappingPK;

public class EmpDeptMappingDao {

	

	public void create(int empId, int deptId, Session session) throws Exception {

		EmpDeptMapping empDept = new EmpDeptMapping();
		EmpDeptMappingPK pk = new EmpDeptMappingPK();
		pk.setDeptId(deptId);
		pk.setEmpId(empId);
		empDept.setId(pk);
		// Audit fields
		empDept.setCreatedBy(System.getProperty("user.name"));
		empDept.setCreatedTime(System.currentTimeMillis());
		empDept.setUpdatedBy(System.getProperty("user.name"));
		empDept.setUpdatedTime(System.currentTimeMillis());
		empDept.setIsDeleted(false);
		session.save(empDept);
	}

}
