package com.tringapps.roomreservation.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.model.Employee;
import com.tringapps.roomreservation.dto.EmployeeDTO;

@SuppressWarnings("all")

public class EmployeeDao {

	Employee employee = new Employee();

	public Employee createEmployee(EmployeeDTO dto, Session session) throws Exception {

		employee.setName(dto.getName());
		employee.setEmail(dto.getEmail());
		employee.setPassword(dto.getPassword());
		employee.setDesignation(dto.getDesignation());
		employee.setSalt(dto.getSalt());
		employee.setIsDeleted(false);

		// Audit fields
		employee.setCreatedBy(System.getProperty("user.name"));
		employee.setCreatedTime(System.currentTimeMillis());
		employee.setUpdatedBy(System.getProperty("user.name"));
		employee.setUpdatedTime(System.currentTimeMillis());
		return (Employee) session.merge(employee);
//		session.save(employee);
//		return employee;
	}

	public void updateEmployee(EmployeeDTO dto, Session session) throws Exception {

		Employee employee = findClass(dto.getEmail(), session);
		if (employee != null) {
			employee.setName(dto.getName());
			employee.setDesignation(dto.getDesignation());
			employee.setPassword(dto.getPassword());
			employee.setSalt(dto.getSalt());

			// Audit fields
			employee.setUpdatedBy(System.getProperty("user.name"));
			employee.setUpdatedTime(System.currentTimeMillis());
			session.update(employee);
		}
	}

	public Employee loginEmployee(String email, Session session) throws Exception {
		return findClass(email, session);
	}
	
	public void deleteEmployee(String email, Session session) throws Exception {
		Employee employee = findClass(email, session);
		if(employee != null) {
			employee.setIsDeleted(true);
			// Audit fields
			employee.setUpdatedBy(System.getProperty("user.name"));
			employee.setUpdatedTime(System.currentTimeMillis());
			session.update(employee);
		}
	}
	
	private Employee findClass(String email, Session session) throws Exception {
		String queryString = "FROM Employee where email = '"+email+"'";
		Query query = session.createQuery(queryString);
		Employee employee = (Employee) query.getSingleResult();
		return employee;
	}
}
