package com.tringapps.roomreservation.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.dto.LoginDto;
import com.tringapps.roomreservation.model.UserSession;

public class SessionDao {

	public void createSession(LoginDto dto, Session session) throws Exception {
		
		UserSession sessionModel = new UserSession();
		sessionModel.setUserEmail(dto.getEmail());
		sessionModel.setUserName(dto.getUserName());
		sessionModel.setAuthId(dto.getAuthId());
		sessionModel.setDeviceToken(dto.getDeviceToken());
		sessionModel.setDeviceType(dto.getDeviceType());
		sessionModel.setLogginTime(System.currentTimeMillis());

		// Audit fields
		sessionModel.setCreatedBy(System.getProperty("user.name"));
		sessionModel.setCreatedTime(System.currentTimeMillis());
		sessionModel.setUpdatedBy(System.getProperty("user.name"));
		sessionModel.setUpdatedTime(System.currentTimeMillis());
		session.save(sessionModel);
	}
	
	public UserSession findSession(String authId, Session session) throws Exception {
		
		String queryString = "FROM UserSession where authId = '"+authId+"'";
		Query query = session.createQuery(queryString);
		UserSession user = (UserSession) query.getSingleResult();
		return user;
	}
	
	public void logout(String email, Session session) throws Exception {
		
		UserSession user = findSession(email, session);
		session.delete(user);
		session.flush();
	}

}
