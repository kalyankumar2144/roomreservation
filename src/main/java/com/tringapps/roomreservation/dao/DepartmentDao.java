package com.tringapps.roomreservation.dao;

import java.sql.Date;
import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.model.Department;
import com.tringapps.roomreservation.dto.DepartmentDTO;

@SuppressWarnings("all")

public class DepartmentDao {
	public void createDepartment(DepartmentDTO deptDto,Session session){

		Department dept = new Department();
		dept.setName(deptDto.getName());
		// Audit fields
		dept.setCreatedBy(System.getProperty("user.name"));
		dept.setCreatedTime(System.currentTimeMillis());
		dept.setUpdatedBy(System.getProperty("user.name"));
		dept.setUpdatedTime(System.currentTimeMillis());
		session.save(dept);
	}

	public void updateDepartment(DepartmentDTO dto, Session session) throws Exception {
		Department department = findDepartmentById(dto, session);
		if (department != null) {
			department.setName(dto.getName());
			department.setUpdatedBy(System.getProperty("user.name"));
			department.setUpdatedTime(System.currentTimeMillis());
			updateDepartmentRecord(department, session);
		} else {
			throw new Exception("Record not found");
		}
	}

	private void updateDepartmentRecord(Department department,Session session){
		try {
			session.saveOrUpdate(department);
			session.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	public Department findDepartmentById(DepartmentDTO dto, Session session) throws Exception {
		try {
			Query query = session.createQuery("from Department d where d.departmentId = :id");
			query.setParameter("id", dto.getName());
			ArrayList queryList = (ArrayList) query.list();
			if (queryList != null && queryList.isEmpty()) {
				return null;
			} else {
				System.out.println("List is not empty");
				Department department = (Department) queryList.get(0);
				return department;
			}
		} catch (Exception e) {
			//e.printStackTrace();
			throw e;
		} 
	}
	
	public void deleteDepartment(DepartmentDTO dto, Session session) throws Exception {
		Department department = findDepartmentById(dto, session);
		if (department != null) {
			deleteDepartmentRecord(department, session);
		} else {
			throw new Exception("Record not found");
		}
	}
	
	private void deleteDepartmentRecord(Department department, Session session) throws Exception {
		try {
			session.delete(department);
			session.flush();
		} catch (Exception e) {
			//e.printStackTrace();
			throw e;
		}
	}

}
