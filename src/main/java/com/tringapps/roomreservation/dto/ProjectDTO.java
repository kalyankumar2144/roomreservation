package com.tringapps.roomreservation.dto;

public class ProjectDTO {

	private String name;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}	
}
