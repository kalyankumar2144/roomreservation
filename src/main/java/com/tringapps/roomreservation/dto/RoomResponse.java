package com.tringapps.roomreservation.dto;

import java.util.ArrayList;

import com.tringapps.roomreservation.model.Room;

public class RoomResponse {

	private ArrayList<Room> roomList;

	public ArrayList<Room> getRoomList() {
		return roomList;
	}

	public void setRoomList(ArrayList<Room> roomList) {
		this.roomList = roomList;
	}
	
}
