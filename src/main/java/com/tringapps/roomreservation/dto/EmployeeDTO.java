package com.tringapps.roomreservation.dto;

import java.util.List;

public class EmployeeDTO {

	private String name;
	private String email;
	private String password;
	private String designation;
	private String salt;
	private List<Integer> deptIdList;
	private List<Integer> projectIdList;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public List<Integer> getDeptIdList() {
		return deptIdList;
	}
	public void setDeptIdList(List<Integer> deptIdList) {
		this.deptIdList = deptIdList;
	}
	public List<Integer> getProjectIdList() {
		return projectIdList;
	}
	public void setProjectIdList(List<Integer> projectIdList) {
		this.projectIdList = projectIdList;
	}
	public String getSalt() {
		return salt;
	}
	public void setSalt(String salt) {
		this.salt = salt;
	}
	
}
