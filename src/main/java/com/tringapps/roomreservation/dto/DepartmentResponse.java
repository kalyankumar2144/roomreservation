package com.tringapps.roomreservation.dto;

import java.util.ArrayList;

public class DepartmentResponse {

	private ArrayList<String> departmentList;

	public ArrayList<String> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(ArrayList<String> departmentList) {
		this.departmentList = departmentList;
	}
	
}
