package com.tringapps.roomreservation.dto;

import java.util.List;

import com.tringapps.roomreservation.helpers.CustomResponse;

public class LoginResponse extends CustomResponse {

	public LoginResponse(String statusMessage) {
		super(statusMessage);
		// TODO Auto-generated constructor stub
	}
	
	private String name;
	private String email;
	private String designation;
	private List<Integer> deptIdList;
	private List<Integer> projectIdList;
	private String authId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public List<Integer> getDeptIdList() {
		return deptIdList;
	}
	public void setDeptIdList(List<Integer> deptIdList) {
		this.deptIdList = deptIdList;
	}
	public List<Integer> getProjectIdList() {
		return projectIdList;
	}
	public void setProjectIdList(List<Integer> projectIdList) {
		this.projectIdList = projectIdList;
	}
	public String getAuthId() {
		return authId;
	}
	public void setAuthId(String authId) {
		this.authId = authId;
	}
	
}
