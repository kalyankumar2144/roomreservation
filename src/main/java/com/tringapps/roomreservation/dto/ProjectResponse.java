package com.tringapps.roomreservation.dto;

import java.util.ArrayList;

public class ProjectResponse {

	private ArrayList<String> projectList;

	public ArrayList<String> getProjectList() {
		return projectList;
	}

	public void setProjectList(ArrayList<String> projectList) {
		this.projectList = projectList;
	}	
	
}
