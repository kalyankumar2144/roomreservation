package com.tringapps.roomreservation.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.tringapps.roomreservation.dao.BookingDao;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.dto.BookingDTO;

public class BookingService {

	
	public void createBooking(BookingDTO bookingDto){
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		BookingDao bookingDao = new BookingDao();
		bookingDao.createBooking(bookingDto, session);
		session.getTransaction().commit();
		session.close();
	}
}
