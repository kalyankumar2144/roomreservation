package com.tringapps.roomreservation.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.tringapps.roomreservation.dao.EmpDeptMappingDao;
import com.tringapps.roomreservation.dao.EmpProjectMappingDao;
import com.tringapps.roomreservation.dao.EmployeeDao;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.model.Employee;
import com.tringapps.roomreservation.dto.EmployeeDTO;

public class EmployeeService {

	EmployeeDao dao = new EmployeeDao();

	public void createEmployee(EmployeeDTO dto) throws Exception {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Employee employee = dao.createEmployee(dto, session);
		session.flush();

		EmpDeptMappingDao mapDao = new EmpDeptMappingDao();
		for (Integer deptId : dto.getDeptIdList()) {
			mapDao.create(employee.getEmployeeId(), deptId, session);
		}

		EmpProjectMappingDao projDao = new EmpProjectMappingDao();
		for (Integer projId: dto.getProjectIdList()) {
			projDao.create(employee.getEmployeeId(), projId, session);
		}
		session.getTransaction().commit();
		session.close();
	}

	public void updateEmployee(EmployeeDTO dto) throws Exception {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		dao.updateEmployee(dto, session);
		session.getTransaction().commit();
		session.close();
	}

	public Employee findEmployee(String email) throws Exception {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		Employee employee = dao.loginEmployee(email, session);
		session.getTransaction().commit();
		session.close();
		return employee;
	}

	public void deleteEmployee(String email) throws Exception {

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		dao.deleteEmployee(email, session);
		session.getTransaction().commit();
		session.close();
	}

}
