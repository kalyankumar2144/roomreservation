package com.tringapps.roomreservation.service;

import java.util.ArrayList;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.dao.DepartmentDao;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.dto.DepartmentDTO;

public class DepartmentService {

	public void createDepartment(DepartmentDTO deptDto){

		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		DepartmentDao departmentDao = new DepartmentDao();
		departmentDao.createDepartment(deptDto, session);
		session.getTransaction().commit();
		session.close();

	}

	public ArrayList getDepartments() {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction(); //Again Open the transaction of the session object

		String queryString = "Select name FROM Department where is_deleted ="+false;
		Query query = session.createQuery(queryString);
		ArrayList departmentList = (ArrayList) query.list();
		session.getTransaction().commit();
		session.close();
		return departmentList;

	}

	public void updateDepartment(DepartmentDTO deptDto) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		DepartmentDao departmentDao = new DepartmentDao();
		try {
			departmentDao.updateDepartment(deptDto, session);
			session.getTransaction().commit();
		} catch (Exception e){
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
	}
	
	public void deleteDepartment(DepartmentDTO deptDto) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		DepartmentDao departmentDao = new DepartmentDao();
		try {
			departmentDao.deleteDepartment(deptDto, session);
			session.getTransaction().commit();
		} catch (Exception e){
			e.printStackTrace();
			throw e;
		} finally {
			session.close();
		}
	}
}
