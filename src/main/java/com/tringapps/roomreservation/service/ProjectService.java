package com.tringapps.roomreservation.service;

import java.util.ArrayList;
import java.util.Iterator;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import com.tringapps.roomreservation.dao.ProjectDao;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.model.Project;
import com.tringapps.roomreservation.dto.ProjectDTO;

@SuppressWarnings("all")

public class ProjectService {
	
	ProjectDao dao = new ProjectDao();
	
	public void create(ProjectDTO project) throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		dao.create(project, session);
		session.getTransaction().commit();
		session.close();
	}
	
	public ArrayList getProjects() throws Exception {
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction(); //Again Open the transaction of the session object

		String queryString = "select name FROM Project where is_deleted ="+false;
		Query query = session.createQuery(queryString);
		ArrayList projects = (ArrayList) query.list();
        session.getTransaction().commit();
		session.close();
		return projects;

	}
	
	public void delete(ProjectDTO dto) throws Exception {
		
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction(); //Again Open the transaction of the session object
		dao.delete(session, dto.getName());
        session.getTransaction().commit();
		session.close();
	}

}
