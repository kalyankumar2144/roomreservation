package com.tringapps.roomreservation.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.tringapps.roomreservation.dao.SessionDao;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.dto.LoginDto;
import com.tringapps.roomreservation.model.UserSession;

public class SessionService {

	public void createSession(LoginDto dto) throws Exception {
		SessionDao dao = new SessionDao();
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		dao.createSession(dto, session);
		session.flush();
		session.getTransaction().commit();
		session.close();
	}

	public UserSession findSession(String authId) throws Exception {
		SessionDao dao = new SessionDao();
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		UserSession userSession = dao.findSession(authId, session);
		session.getTransaction().commit();
		session.close();
		return userSession;
	}
	
	public void logout(String email) throws Exception {
		SessionDao dao = new SessionDao();
		SessionFactory sf = HibernateUtil.getSessionFactory();
		Session session = sf.openSession();
		session.beginTransaction();
		dao.logout(email, session);
		session.getTransaction().commit();
		session.close();
	}
}
