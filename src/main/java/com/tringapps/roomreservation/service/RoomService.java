package com.tringapps.roomreservation.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import com.tringapps.roomreservation.datasource.utils.HibernateUtil;
import com.tringapps.roomreservation.model.Room;
import com.tringapps.roomreservation.dto.RoomDto;
import com.tringapps.roomreservation.dao.RoomDao;

@SuppressWarnings("all")

public class RoomService {

	Session getSession() {
		SessionFactory sessoinFactory = HibernateUtil.getSessionFactory();
		return sessoinFactory.openSession();
	}
	
	public Room createRoomEntry(RoomDto roomDto) {
		Session session  = getSession();
		session.beginTransaction();
		RoomDao roomDao = new RoomDao();
		Room room = roomDao.insertRoomDetails(roomDto, session);
		session.getTransaction().commit();
		session.close();
		return room;
	}
	
	public Room getRoomDetail(int roomNumber) {
		Session session  = getSession();
		session.beginTransaction();
		RoomDao roomDao = new RoomDao();
		Room room = roomDao.getRoomDetail(roomNumber, session); 
		session.close();
		return room;
	}
	
	public List getRoomDetails() {
		Session session  = getSession();
		session.beginTransaction();
		RoomDao roomDao = new RoomDao();
		List<Room> roomList = roomDao.getRoomDetails(session); 
		session.close();
		return roomList;
	}
	
	public Room updateRoomDetail(RoomDto roomDto) {
		Session session  = getSession();
		session.beginTransaction();
		RoomDao roomDao = new RoomDao();
		Room room = roomDao.updateRoomDetail(roomDto, session);
		session.getTransaction().commit();
		session.close();
		return room;
	}
	
	public Room deleteRoomDetail(int roomNumber) {
		Session session  = getSession();
		session.beginTransaction();
		RoomDao roomDao = new RoomDao();
		Room room = roomDao.deleteRoomDetail(roomNumber, session); 
		session.getTransaction().commit();
		session.close();
		return room;
	}
	
}
