package com.tringapps.roomreservation.domain.model;

public class ErrorResponse {

	public String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
