package com.tringapps.roomreservation.filter;

//public class AuthenticationFilter {
//
//}

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sun.jersey.core.util.Base64;
import com.tringapps.roomreservation.model.UserSession;
import com.tringapps.roomreservation.service.SessionService;

public class AuthenticationFilter implements javax.servlet.Filter {
	
	private long TWO_DAYS_IN_MINUTES = 2 * 24 * 60 * 1000;
	public static final String AUTHENTICATION_HEADER = "authId";
	public static final String DEVICE_TYPE_HEADER = "deviceType";
	public static final String DEVICE_TOKEN_HEADER = "deviceToken";

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filter) throws IOException, ServletException {
		
		if (request instanceof HttpServletRequest) {
			
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			String authCredentials = httpServletRequest
					.getHeader(AUTHENTICATION_HEADER);
		    String reqUrl = httpServletRequest.getRequestURL().toString();
			System.out.println("authCredentials ==== "+authCredentials+ "\nreqUrl ==="+reqUrl);
			
			if(isAuthenticated(authCredentials, httpServletRequest) || skipValidationUrls(reqUrl)) {
				filter.doFilter(request, response);
			} else {
				HttpServletResponse httpServletResponse = (HttpServletResponse) response;
				httpServletResponse
						.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			}
		}
	}
	
	private boolean skipValidationUrls(String url) {
		return url.contains("user/login") || url.contains("employee/signup");
	}
	
	private boolean isAuthenticated(String authCredentials, HttpServletRequest request) {

		if (authCredentials == null)
			return false;
		
		try {
			/**** Encrypt authId ****/
			byte[] decodedBytes = Base64.decode(
					authCredentials);
			String usernameAndEmail = new String(decodedBytes, "UTF-8");
			final StringTokenizer tokenizer = new StringTokenizer(usernameAndEmail, "|");
			final String username = tokenizer.nextToken();
			final String email = tokenizer.nextToken();
			final String loginTime = tokenizer.nextToken();
			System.out.println("username ==== "+username+ "\nemail ==="+email+"\nloginTime === "+loginTime);

			/**** Get Session from DB using authId ****/
			SessionService session = new SessionService();
			UserSession user = session.findSession(authCredentials);
			long twoDays =  user.getLogginTime() + TWO_DAYS_IN_MINUTES;

			/**** Get header from request ****/
			Boolean isHeaderValid = request.getHeader(DEVICE_TYPE_HEADER).equals(user.getDeviceType()) && 
					request.getHeader(DEVICE_TOKEN_HEADER).equals(user.getDeviceToken()) &&
					authCredentials.equals(user.getAuthId());
			Boolean isSessionAlive = System.currentTimeMillis() < twoDays;
			Boolean isValidSession = request.getSession().getId() != null && !request.getSession().isNew();
			
			if (isHeaderValid && isSessionAlive && isValidSession) {
			    System.out.println("Login timestamp is valid");
			    return true;
			}
			return false;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}