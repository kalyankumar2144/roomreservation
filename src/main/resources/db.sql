CREATE TABLE `Booking` (
  `booking_id` int(11) NOT NULL AUTO_INCREMENT,
  `booked_by` varchar(45) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `start_time` bigint(20) NOT NULL,
  `end_time` bigint(20) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `Department` (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EmpDeptMapping` (
  `emp_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`emp_id`),
  KEY `fk_2_idx` (`dept_id`),
  CONSTRAINT `fk_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `fk_2` FOREIGN KEY (`dept_id`) REFERENCES `department` (`department_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `Employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(20) NOT NULL,
  `designation` varchar(20) DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(45) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `salt` varchar(45) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `EmpProjectMapping` (
  `project_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(45) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  PRIMARY KEY (`project_id`),
  KEY `fk_1_idx` (`emp_id`),
  KEY `fk_1_idx14` (`emp_id`),
  CONSTRAINT `fk_emp_1` FOREIGN KEY (`emp_id`) REFERENCES `employee` (`employee_id`),
  CONSTRAINT `fk_proj_2` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `Project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


CREATE TABLE `Room` (
  `capacity` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `UserSession` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_id` varchar(45) NOT NULL,
  `login_time` bigint(20) NOT NULL,
  `user_email` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `device_token` varchar(45) DEFAULT NULL,
  `device_type` varchar(45) DEFAULT NULL,
  `created_by` varchar(20) NOT NULL,
  `created_time` bigint(20) NOT NULL,
  `updated_by` varchar(20) NOT NULL,
  `updated_time` bigint(20) NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `auth_id_UNIQUE` (`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

